FROM alpine:3.7
MAINTAINER thulasi<thulasikumar813@gmail.com>
WORKDIR /user/app/hello-docker/
RUN apk add --update bash
RUN apk add --update nodejs nodejs-npm
RUN npm insatll -g http-server
ADD ./user/app/hello-docker
ADD index.html/usr/app/hello-docker/index.html
CMD ["http-server","-s"]

